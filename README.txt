This is a very simple demo application for getting some coding practice with:
- SpringBoot
- Java
- Microservices (NetFlix Eureka)
- REST
- JSON 

The theme is a guitar inventory management system.

If the make happens to be "Gibson", the model is submitted to a second microservice which returns some historical information.

As will be seen from the logs in the guitar client, the REST API creates some guitars, retrieves the Fender, updates (removes repair information) and then deletes it.

The following repositories are in scope of this project:

https://bitbucket.org/sam_laidler/service-reg
https://bitbucket.org/sam_laidler/guitar-service
https://bitbucket.org/sam_laidler/gibson-service
https://bitbucket.org/sam_laidler/guitar-client
https://bitbucket.org/sam_laidler/guitardependencies
