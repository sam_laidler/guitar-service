package sam.laidler;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import sam.laidler.persistence.PersistenceInterface;
import sam.laidler.stubs.PersistenceStub;

@Configuration
public class AppConfig {
	@Bean
	public PersistenceInterface getPersistenceLayer() {
		return new PersistenceStub();
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
}
