package sam.laidler.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import sam.laidler.domain.Guitar;
import sam.laidler.persistence.PersistenceInterface;

@RestController
@RequestMapping(path = "/guitars", produces = "application/json")
public class GuitarController {
	@Autowired
	PersistenceInterface persistenceInterface;
	
	private RestTemplate restTemplate;

	private final String GIBSON = "Gibson";
	
	public GuitarController(@LoadBalanced RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@PostMapping(consumes = "application/json")
	public Guitar create(@RequestBody Guitar guitar) {
		if (GIBSON.matches(guitar.getMake())) {
			guitar.setHistory(restTemplate.getForObject("http://gibson-service/gibson-service/{cost}", String.class,
					guitar.getModel()));
		}
		else {
			guitar.setHistory("No history available");
		}
		persistenceInterface.create(guitar);
		
		return guitar;
	}

	@GetMapping(path = "/{id}")
	public Guitar read(@PathVariable("id") int id) {
		return persistenceInterface.read(id);
	}
	
	@GetMapping
	public ArrayList<Guitar> readAll() {
		return persistenceInterface.readAll();
	}

	@PostMapping(path = "/update/{id}")
	public void update(@PathVariable("id") int id, @RequestBody Guitar guitar) {
		persistenceInterface.update(id, guitar);
	}

	@PostMapping(path = "/delete/{id}")
	public void delete(@PathVariable("id") int id) {
		persistenceInterface.delete(id);
	}
}
