package sam.laidler.persistence;

import java.util.ArrayList;

import sam.laidler.domain.Guitar;

public interface PersistenceInterface {
	public void create(Guitar guitar);
	public Guitar read(int id);
	public ArrayList<Guitar> readAll();
	public void update(int id, Guitar guitar);
	public void delete(int id);
}
