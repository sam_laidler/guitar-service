package sam.laidler.stubs;

import java.util.ArrayList;

import sam.laidler.domain.Guitar;
import sam.laidler.domain.Repair;
import sam.laidler.persistence.PersistenceInterface;

public class PersistenceStub implements PersistenceInterface {
	private ArrayList<Guitar> guitars = new ArrayList<>();
	
	public void create(Guitar guitar) {
		guitar.setId(guitars.size());
		
		ArrayList<Repair> repairs = guitar.getRepairs();
		if (repairs != null && repairs.size() > 0) {
			repairs.forEach(r -> r.setId(repairs.indexOf(r)));
		}
		
		guitars.add(guitar);
	}
	
	public Guitar read(int id) {
		for (Guitar guitar : guitars) {
			if (guitar.getId() == id) {
				return guitar;
			}
		}
		return null;
	}
	
	public ArrayList<Guitar> readAll() {
		return guitars;
	}
	
	public void update(int id, Guitar guitar) {
		guitars.set(id, guitar);
	}
	
	public void delete(int id) {
		guitars.remove(id);
	}
}
